#!/bin/bash

#Jesus Reyes Castillo

time=$(ps -ex | awk '{print $4}')

Timeminutos=0
Timehoras=0
Lines=0
Timedia=0

for line in $time ; do
    ((lines++))

    if ! [ $lines -eq 1 ]; then
        horas=$(echo $line | cut -d ":" -f1)
        minutos=$(echo $line | cut -d ":" -f2)
        Timehoras=$(echo "$Timehoras + $horas" | bc)
        Timeminutos=$(expr $Timeminutos + $minutos)
    fi
done


while [ $Timehoras -ge "24" ]; do
    let Timehoras=Timehoras-24
    let Timedia=Timedia+1
done


while [ $Timeminutos -ge "60"  ]; do
    Timeminutos=$(echo "$Timeminutos - 60" | bc)
    Timehoras=$(echo "$Timehoras + 1" | bc)
done



if [ $Timedia -gt 0 ]; then
   echo " Dias " $Timedia
fi


if [ $Timehoras -gt 0 ]; then
   echo " Horas " $Timehoras
fi


if [ $Timeminutos -gt 0 ]; then
   echo " Minutos " $Timeminutos
fi

exit 0
