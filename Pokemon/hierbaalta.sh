#!/bin/bash

#Jesus Reyes

if ! [ $# -eq 2 ]; then
    echo "ERROR"
fi

if [ $2 = "Normal" ]; then
    a="2"
elif [ $2 = "Fire" ]; then
    a="3"
elif [ $2 = "Water" ]; then
    a="4"
elif [ $2 = "Electric" ]; then
    a="5"
elif [ $2 = "Grass" ]; then
    a="6"
elif [ $2 = "Ice" ]; then
    a="7"
elif [ $2 = "Fighting" ]; then
    a="8"
elif [ $2 = "Poison" ]; then
    a="9"
elif [ $2 = "Ground" ]; then
    a="10"
elif [ $2 = "Flying" ]; then
    a="11"
elif [ $2 = "Psychic" ]; then
    a="12"
elif [ $2 = "Bug" ]; then
    a="13"
elif [ $2 = "Rock" ]; then
    a="14"
elif [ $2 = "Ghost" ]; then
    a="15"
elif [ $2 = "Dragon" ]; then
    a="16"
elif [ $2 = "Dark" ]; then
    a="17"
elif [ $2 = "Steel" ]; then
    a="18"
elif [ $2 = "Fairy" ]; then
    a="19"
else
    echo "Escribe los argumentos"
fi
ataque=$(cat chart.csv | grep $1 | tail -n1 | cut -d, -f$a)
echo "El ataque de $1 y de $2 es : $ataque"

exit 0
