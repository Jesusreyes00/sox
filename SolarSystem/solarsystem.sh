#!/bin/bash

#Jesus Reyes Castillo

usuario=$1
accion=$2
fecha=$(date +%d\-%m\-%y)
fsaturn=$(date +%y\-%m\-%d)
ruta=$3

cat /etc/passwd > users.txt

grep  "$usuario" users.txt > hay.txt

lineas=$(wc -l < hay.txt)

echo "$lineas"

if [ $lineas == 0 ]; then
	echo "Error* -El usuario no existe"
else
	echo "El usuario si  existe"
	case $accion in

	replenish)
		echo "Replenish"
		if [ -d "/home/$usuario/solarsystem" ]; then
			echo "La carpeta solarsystem ya existe"
		else
			mkdir /home/$usuario/solarsystem
			mkdir /home/$usuario/solarsystem/mercury
			echo $usuario $fecha > /home/$usuario/solarsystem/mercury/control-planet.txt

			mkdir /home/$usuario/solarsystem/venus
			echo $usuario $fecha > /home/$usuario/solarsystem/venus/control-planet.txt

			mkdir /home/$usuario/solarsystem/mars
			echo $usuario $fecha > /home/$usuario/solarsystem/mars/control-planet.txt

			mkdir /home/$usuario/solarsystem/jupiter
			echo $usuario $fecha > /home/$usuario/solarsystem/jupiter/control-planet.txt

			mkdir /home/$usuario/solarsystem/saturn

			mkdir /home/$usuario/solarsystem/uranus
			echo $usuario $fecha > /home/$usuario/solarsystem/uranus/control-planet.txt

			mkdir /home/$usuario/solarsystem/neptune
			echo $usuario $fecha > /home/$usuario/solarsystem/neptune/control-planet.txt

			tree /home/$usuario/solarsystem > /home/$usuario/solarsystem/saturn/environment-$usuario-$fsaturn.txt


			echo "Se han creado los siguientes planetas"
			echo "/home/$usuario/solarsystem"
			echo "---mercury---"
			echo "---venus---"
			echo "---mars---"
			echo "---jupiter---"
			echo "---saturn---"
			echo "---uranus---"
			echo "---neptune---"

fi
	;;

	test)

		mkdir /home/$usuario/solarsystem
                mkdir /home/$usuario/solarsystem/mercury
                echo $usuario $fecha > /home/$usuario/solarsystem/mercury/control-planet.txt

                mkdir /home/$usuario/solarsystem/venus
                echo $usuario $fecha > /home/$usuario/solarsystem/venus/control-planet.txt

                mkdir /home/$usuario/solarsystem/mars
                echo $usuario $fecha > /home/$usuario/solarsystem/mars/control-planet.txt

                mkdir /home/$usuario/solarsystem/jupiter
                echo $usuario $fecha > /home/$usuario/solarsystem/jupiter/control-planet.txt

                mkdir /home/$usuario/solarsystem/saturn

                mkdir /home/$usuario/solarsystem/uranus
                echo $usuario $fecha > /home/$usuario/solarsystem/uranus/control-planet.txt

                mkdir /home/$usuario/solarsystem/neptune
                echo $usuario $fecha > /home/$usuario/solarsystem/neptune/control-planet.txt

                tree /home/$usuario/solarsystem  > /home/$usuario/solarsystem/saturn/environment-$usuario-$fsaturn.txt

                if [ -e /home/$usuario/solarsystem/$ruta ]; then
                        echo "---------------------------"
                        echo "La carpeta esta en su sitio"
                        echo "---------------------------"
                        if [ -r $usuario ]; then
                                echo "Tiene permisos"
                                echo "Se puede leer como administrador"
                        else
                                echo "No tienes permisos pero ahora te los concedo"
                                chmod +r-x /solarsystem
                        fi

                        if [ $usuario == "root" ]; then

                                if [ -r $usuario ]; then
					echo "----------------------"
                                        echo "El usuario root existe"
					echo "----------------------"
                                        if [ -w /home/$usuario/solarsystem ]; then
                                                echo "Todas las carpetas tienen permisos de lectura"

                                        else
						echo "-----------------------------"
                                                echo "No tienes permisos de lectura"
						echo "-----------------------------"
                                        fi

                                else
					echo "-------------------------"
                                        echo "El usuario root no existe"
					echo "-------------------------"
                                fi


			fi

                else
                        echo "------------------------------"
                        echo "La carpeta no esta en su lugar"
			echo "------------------------------"

		fi


	;;

	clean)

		if [ -d $ruta ]; then

			echo "Si existe y esta en su sitio"
			sudo rm -r $ruta

			echo "La ruta indicada se ha eliminado con sus subcarpetas y directorios"
		else
			echo "----------------------------------------------------"
			echo "El directorio no es válido porque no esta en la ruta"
			echo "----------------------------------------------------"
		fi
	;;

	esac

fi
exit 0
