# PHPLDAPADMIN

 El primer paso es instalar el Sldap:

![](slapd.png)

Despues instalamos el phpldapadmin con el siguiente comando:

![](ñiauum.png)

Tendremos que configurarlo con:

```{.example}
dpkg-reconfigure slapd
```

Introducimos el nombre de dominio DNS:

![](dns.png)

Y también el nombre de la organización:

![](banda%20organizada.png)

Al final de la configuración, le diremos "no" a borrar la base de datos y "yes" a mover la base antigua.

Descargaremos el .zip de github con el comando:

```{.example}
wget https://github.com/leenooks/phpLDAPadmin/archive/refs/tags/1.2.6.4.zip
```

El .zip lo descomprimiremos con:

```{.example}
unzip 1.2.6.4.zip
```

El siguiente paso será borrar el contenido de /usr/share/phpldapadmin/, para así despues copiar el contenido del .zip descomprimido y copiarlo en dicho directorio.

Ahora nos meteremos en /etc/phpldapadmin/config.php y cambiaremos así el contenido:

![](Cambiamos%20eso.png)

Lo siguiente será iniciar sesion en el server (http://IP/phpldapadmin/):

![](Ya%20va%20el%20server.png)

Una vez dentro del server le daremos a "crear un objeto hijo" y poco a poco crearemos el siguiente esquema:

![](Creamos%20esquema.png)

Cuando hayamos terminado eso, volveremos a la maquina para poner el siguiente comando y ver que todo funciona correctamente:

![](Ponemos%20ese%20comando%20para%20comprobar.png)

Y vemos que todo funciona perfecto:

![](Vemos%20q%20todo%20bn.png)