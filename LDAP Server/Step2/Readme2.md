# PHPLDAPADMIN 2

 El primer paso es instalar esto en el cliente:

![](primerafoto.png)

Ahora le decimos al cliente LDAP el server que usará:

![](segunda.png)

Ponemos el nombre del servidor:

![](tercera.png)

Le indicamos que use la version 3:

![](version.png)

Le decimos que si:

![](si.png)

Y ahora le indicaremos que no:

![](no.png)

Le indicamos la cuenta que tendrá el ROOT:

![](septima.png)

Ahora ponemos la contraseña del LDAP:

![](contrase%C3%B1a.png)

En el servidor, instalaremos gnutls-bin y ssl-cert:

![](instalamoseso.png)

Generaremos la clave privada:

![](generamoslaclave.png)

Crearemos el fichero /etc/ssl/ca.info para definir el CA:

![](creamosesefichero.png)

Creamos el certificado firmado:

![](creamoselcafirmao.png)

Añadimos el certificado:

![](certificados.png)

Generamos la clave privada:

![](laprivada.png)

Creamos el fichero /etc/ssl/ldap01.info:

![](creamoseso.png)

Ahora creamos el certificado pero del servidor:

![](certool.png)

Ponemos los propietarios y los permisos:

![](permisosyeso.png)

Creamos el fichero certinfo.ldif y añadimos eso:

![](creamoseseficheron.png)

Usamos el ldapmodify para que el LDAP actualize los cambios:

![](ldapmodify.png)

Vamos al fichero /etc/default/sldap y debajo de la linea "SLDAP_SERVICES" añadimos el ldaps:

![](cambiareson.png)

Ahora reiniciamos servicio:

```{.example}
sudo service slapd restart
```
Ahora para probar, cambiamos el /etc/hosts:

![](hosts.png)

Ahora si, probamos:

![](anonymous.png)

Lo siguiente será instalar libpam-sss y libnss-sss:

![](libpam.png)

Crearemos el fichero /etc/sssd/sssd.conf y añadiremos eso:

![](copiarpegar.png)

En el cliente descargaremos el certificado con este comando:

![](cacertificate.png)

Al intentar validarlo nos dará error:

![](error.png)

Para solucionar el error, meteremos el certificado en /etc/ssl/certs/ldapcacert.crt

![](ahi.png)

Ahora ya no nos da error:

![](connected.png)

En el fichero /etc/ldap/ldap.conf cambiaremos el directorio de TLS_CACERT:

![](modificamos.png)

Cambiaremos el dueño, grupo y permisos de los ficheros de /etc/sssd:

![](due%C3%B1o.png)

Instalamos el paquete libsss-simpleifp0:

![](instalamos.png)

Comprobamos que va correctamente:

![](sssd.png)

En el archivo "/etc/pam.d/common-session", debajo de session optional pam_sss.so añadimos eso:

![](lala.png)

Hacemos el getent passwd y nos saldrán los goblins:

![](goblins.png)

Y vemos que logamos con ellos:

![](ultima.png)